/*-
 * Copyright 2020 elementary, Inc (https://elementary.io)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authored by: Corentin Noël <tintou@noel.tf>
 */

/**
 * = Calendar authentication plugin for gSSO single sign-on service =
 *
 * The Calendar plugin is a simple plugin to acquire and store the credentials.
 * It currently only handle credentials for CalDAV and ICS.
 *
 * The plugin implements the standard {@link GSignond.Plugin} interface, and after instantiating
 * a plugin object all interactions happen through that interface.
 *
 * {@link GSignond.Plugin.type} property of the plugin object is set to "calendar".
 *
 * {@link GSignond.Plugin.mechanisms} property of the plugin object is set to "caldav".
 *
 * ==Token and its parameters in {@link GSignond.Plugin.response_final} signal==
 *
 * {@link GSignond.Plugin.response_final} signal concludes the process and returns a
 * {@link GSignond.Dictionary} parameter that contains the access parameters:
 *
 *  * "URL" //(mandatory)// - the caldav url
 *  * "Security" //(mandatory)// - the security kind, can be one of "None" or "SSL/TLS"
 *  * "User" //(mandatory)// - the username
 *  * "Password" //(mandatory)// - the password
 *
 * @see GSignond.Plugin
 */
public class GSignond.CalendarPlugin : GLib.Object, GSignond.Plugin {
    public string type { owned get { return "calendar"; } }
    public string[] mechanisms { owned get { return {"caldav", null}; } }

    public void cancel () {
        var signond_error = new GSignond.Error.SESSION_CANCELED ("Session canceled");
        error (signond_error);
    }

    public void request_initial (GSignond.SessionData session_data, GSignond.Dictionary token_cache, string mechanism) {
        var username = session_data.get_username ();
        var secret = session_data.get_secret ();
        if (secret != null) {
            var response = new GSignond.SessionData.from_variant (token_cache.to_variant ());
            if (username != null) {
                response.set_username (username);
            }

            response.set_secret (secret);
            response_final (response);
            return;
        }

        var user_action_data = new GSignond.SignonuiData ();
        if (username == null) {
            user_action_data.set_query_username (true);
        } else {
            user_action_data.set_query_username (false);
            user_action_data.set_username (username);
        }

        user_action_data.set_boolean ("AskCalendarSettings", true);
        user_action_data.set_query_password (true);
        user_action_required (user_action_data);
    }

    public void request (GSignond.SessionData session_data) {
        var err = new GSignond.Error.WRONG_STATE ("Calendar plugin doesn't support request");
        error (err); 
    }

    public void user_action_finished (GSignond.SignonuiData ui_data) {
        GSignond.SignonuiError query_error;
        if (ui_data.get_query_error (out query_error) == false) {
            var err = new GSignond.Error.USER_INTERACTION ("user_action_finished did not return an error value");
            error (err);
            return;
        }

        if (query_error == GSignond.SignonuiError.NONE) {
            var response = new GSignond.SessionData ();
            response.set_string ("URL", ui_data.get_string ("URL"));
            response.set_string ("Security", ui_data.get_string ("Security"));
            response.set_string ("User", ui_data.get_string ("User"));
            response.set_string ("Password", ui_data.get_string ("Password"));

            response.set_username (ui_data.get_string ("User"));
            response.set_secret (ui_data.get_string ("Password"));
            store (response);
            response_final (response);
        } else if (query_error == GSignond.SignonuiError.CANCELED) {
            var err = new GSignond.Error.SESSION_CANCELED ("Session canceled");
            error (err);
        } else {
            var err = new GSignond.Error.USER_INTERACTION ("userActionFinished error: %d", query_error);
            error (err);
        }
    }

    public void refresh (GSignond.SignonuiData session_data) {
        refreshed (session_data);
    }
}
